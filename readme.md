<p align="center"><img src="https://discuzq.cc/img/discuzq.jpg"></p>


## 关于 Discuz! Q

**[Discuz Q 开源非盈利社区](https://discuzq.cc)**

## 安装方法

服务器环境需求为： **PHP 7.2.5+** 和 **MySQL 5.7+**。

## 第一步：下载 Discuz! Q

首先注册[腾讯云帐号](https://cloud.tencent.com)
并
[实名认证](https://console.cloud.tencent.com/developer/auth)，
然后在
[API密钥管理](https://console.cloud.tencent.com/cam/capi)
处新建一个密钥

## Discuz!Q 源码归类


[【后台管理下载】](https://gitee.com/Discuz/discuz-q-web-admin)https://gitee.com/Discuz/discuz-q-web-admin

[【PC端前端下载】](https://gitee.com/Discuz/Discuz-Q-Web)https://gitee.com/Discuz/Discuz-Q-Web

[【H5+小程序下载】](https://gitee.com/Discuz/Discuz-Q-uniapp)https://gitee.com/Discuz/Discuz-Q-uniapp


## 感谢

### 背景故事

`Discuz! Q`项目由于是从 0 到 1，介于我们的目标，如果从第一行代码开始编写，是极为庞大的工程。想想`Discuz!X`，代码量依赖 10 多年的时间的积累，才完善出各种工具类、自己的框架及插件机制等。

在此背景下，我们必须借助开源的力量，才得以快速构建出`Discuz! Q`。以下是整个`Discuz! Q`中所用到的技术栈，在此特别感谢他们：

Discuz! Q 是更轻的，更易变现的，更移动端的，更开放的和更易于二次开发的社区产品。

Discuz! Q 是一套跨端全域的社区工具，内置六大能力：用户能力、内容能力、支付能力、运营能力、通知能力、连接能力；可以设置公开、付费模式，发布包括图文、短视频、附件、话题、评论等内容形式；并支持知识变现，可以内容打赏、设置付费内容、微信支付、分成、提现等；同时还是腾讯云能力输出官方组件，通过 Discuz! Q 的发布，既满足站长开箱即用的社区和腾讯云使用的基本需求，又满足开发者基于 Discuz! Q 开发框架开发各种应用，更多更好的服务好新的站长群体，打造一个百花齐放的全新社区生态。

Discuz! Q 使用主流的框架，前后分离的方式重写了全部代码，数百个接口全部开放，原生的连接微信生态和腾讯云，帮助开发者事半功倍 ；基于 Apache License 2.0 开源协议，开发者无后顾之忧，只需专注于业务场景的落地。

同时，Discuz! Q 提供了全新的 7 大特性。

开源：基于 Apache Licene 2.0 协议，在遵守协议的基础上，您可以自由的使用、修改 和 发布代码。同时，欢迎通过社区提交你的代码，让 Q 拥有你提供的能力 。

分离：Q 的前后端完全分离，后端基于 laravel，前端基于 vue 和 uni-app，易于二次开发和扩展。

多端：Q 原生的支持微信小程序、H5 和 PC 端（开发中），同时，基于 UNI-APP 前端框架的多端扩展能力，开发者可以极低成本的快速构建 IOS、安卓 APP、百度小程序等更多端

接口：数百个接口（仍然在增加中），健全的文档，开发者可轻易、灵活的使用，完成各种应用场景的构建。

轻量：依然是 Discuz!，这次，你既可以搭建轻量化的论坛，也可以构建知识付费、内容变现的圈子或私域流量应用。

变现：Q 内置圈子付费加入、打赏、内容付费、电商（未来），多种变现方式，帮助内容创业者快速启动。

上云：Q 原生接入腾讯云的对象存储、文本安全、图片安全、短信、验证码、实名认证、视频 等产品，借助 Q 背后云的能力，帮助开发者、创业者领先一步。

<p><a href="https://laravel.com/"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="200"></a></p>
<p><a href="https://symfony.com/"><img src="https://symfony.com/images/logos/header-logo.svg" width="200"></a></p>
<p><a href="https://getlaminas.org/"><img src="https://getlaminas.org/images/logo/laminas-foundation-rgb.svg" width="200"></a></p>

[FastRoute](https://github.com/nikic/FastRoute)

[Guzzle](http://guzzlephp.org/)

[thephpleague](https://thephpleague.com/) 

[s9etextformatter](https://s9etextformatter.readthedocs.io/)

[overtrue](https://overtrue.me/)

[intervention](http://image.intervention.io/)

[monolog](https://github.com/Seldaek/monolog)

[whoops](https://github.com/filp/whoops)

[vue](https://vuejs.org/)

[Vant](https://youzan.github.io/vant/#/zh-CN/)

[element-ui](https://element.eleme.cn/#/zh-CN)
